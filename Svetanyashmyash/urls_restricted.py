from django.conf.urls import url, include

from blog_app.views import *
from django.contrib import admin
from django.contrib.auth.views import login

admin.autodiscover()

urlpatterns = [
    url(r'^$', restricted, name='restricted'),
    url(r'^admin_post_pic/$', admin_post_pic),
    url(r'^admin_get_pic/$', admin_get_pic),
    url(r'^admin_del_pic/$', admin_del_pic),
    url(r'^edit/(?P<id>\d+)/$', edit),
    url(r'^edit/$', new),
    url(r'^preview/$', preview),
    url(r'^archive/$', archive),
    url(r'^profile/$', profile),
    url(r'^logout/$', user_logout),
    url(r'^login/$', login, {'template_name': 'blog_app/login.html'}, name='login_django'),
    url(r'^admin/', include(admin.site.urls)),
]
